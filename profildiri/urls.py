from django.urls import path

from . import views

app_name = 'profildiri'

urlpatterns = [
    path('', views.profildiri, name='profildiri'),
]