from django.apps import AppConfig


class ProfildiriConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'profildiri'
