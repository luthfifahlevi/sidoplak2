from django.shortcuts import render,redirect
from django.http import Http404, HttpResponse
from daftar.models import DonorApplication, DonorApplicationStatus, Pasien, Pendonor
# Create your views here.

def profildiri(request):
    if not request.user.is_authenticated or request.user.is_admin():
        raise Http404
    self = request.user
    selfObject = None
    sudahDaftar = None
    status = ""
    statusInt = 0
    if self.role=="PASIEN":
	    selfObject = Pasien.objects.filter(user = self)
	    if len(selfObject) == 1:
	        print(selfObject)
	        tmp = DonorApplication.objects.filter(patient = selfObject[0])
	        status = tmp[0].status.message
	        statusInt = tmp[0].status.status
    elif self.role == "PENDONOR":
        print("halo")
        selfObject = Pendonor.objects.filter(user = self)
        if len(selfObject) == 1:
	        tmp = DonorApplication.objects.filter(donor = selfObject[0])
	        status = tmp[0].status.message
	        statusInt = tmp[0].status.status
    if len(selfObject)>0:
	    selfObject = selfObject[0]
	    sudahDaftar = 1
    else:
	    selfObject = request.user
    print(statusInt)
    context = {
        'statusInt' : statusInt,
    	'selfObject' : selfObject,
    	'status' : status,
    	'sudahDaftar' : sudahDaftar
    }
    return render(request,'profilpage/profilpage.html',context)

