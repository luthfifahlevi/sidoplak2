# SIDOPLAK

[![pipeline status][pipeline-badge]][commits-gl]
[![coverage report][coverage-badge]][commits-gl]

## Daftar Isi

- [Daftar Isi](#daftar-isi)
- [Anggota](#anggota)
- [Link Herokuapp](#link-herokuapp)
- [Tentang Aplikasi](#tentang-aplikasi)
- [Daftar Fitur](#daftar-fitur)


## Anggota

Kelompok C06 RPL

1. Dewangga Putra Sheradhien - 1906293013 (Artikel)
2. Fadli Aulawi Al Ghiffari - 1906285623 (Mendaftarkan diri)
3. Michael Felix Haryono - 1906398326 (Profil Diri dan List Pasien)
4. Muhammad Luthfi Fahlevi - 1906293215 (Forum)
5. Rico Tadjudin - 1906398364 (Verifikasi Pasangan dan Melihat Profil Pasien dan Pendonor)

## Link Herokuapp

Link Herokuapp: [https://sidoplak.herokuapp.com](https://sidoplak.herokuapp.com)

## Tentang Aplikasi

SIDOPLAK atau Sistem Donor Plasma Konvalesesn merupakan suatu media untuk mempertemukan antara Pasien yang membutuhkan donor plasma serta pendonor yang ingin mendonorkan plasma darahnya. Tidak hanya itu, SIDOPLAK sendiri menyediakan fitur tambahan seperti FORUM dan juga ARTIKEL yang dapat membantu pengguna baik dalam berselancar dalam aplikasi ini, informasi tambahan tentang COVID, dan juga berdiskusi dengan sesame pengguna. 

## Daftar Fitur

1. Mendaftar Diri untuk Meminta Donor (UCS ID : 11)
Pada fitur ini, user dapat mendaftarkan diri untuk meminta donor dengan memasukan data diri secara lengkap serta memberikan beberapa prosuder tambahan seperti surat permohonan donor resmi dari faskes.

2. Mendaftar Diri untuk Mendonor Donor (UCS ID : 11)
Pada fitur ini, user dapat mendaftarkan diri untuk melakukan donor dengan memasukan data diri secara lengkap serta memberikan beberapa prosuder tambahan seperti surat pernah positif COVID-19 serta surat sembuh COVID-19.

3. Mengelola artikel (UCS ID : 2)
Fitur ini mengurus tentang seorang admin dapat melakukan create, edit, delete pada artikel-artikel yang ada

4. Melihat daftar pasien (UCS ID : 3)
Fitur ini bertujuan untuk menampilkan data-data pasien yang sedang membutuhkan donor. Fitur ini juga mengatur pasien dengan golongan darah maupun provinsi apa saja yang perlu ditampilkan.

5. Melihat profil diri (UCS ID : 4)
Fitur ini menampilkan data lengkap pengguna sesuai dengan data yang diberikan pengguna serta dapat digunakan untuk melihat status pengguna.

6. Melihat profil pasien dan pendonor (UCS ID : 5)
Fitur ini berfungsi agar admin dapat melihat secara lengkap profil-profil semua pengguna yang ada di dalam aplikasi SIDOPLAK. Tidak hanya itu, admin juga bisa melakukan filter pasien maupun pendonor.

7. Menyetujui pasangan pasien dan pendonor (UCS ID : 6)
Fitur yang ditujukan pada admin, agar admin dapat melakukan verifikasi pasangan pasien dan pendonor yang terjadi. Lalu berdasarkan profil diri yang diberikan, admin dapat menolak dan menerima pasangan tersebut.

8. Mengelola Forum (UCS ID : 7)
Fitur yang mengatur mengenai proses mengelola forum dimana pengguna dapat membuat, membaca, mengubah, dan menghapus postingan di forum diskusi.


[commits-gh]: https://github.com/luthfifahlevi/sidoplak2/commits/develop
[pipeline-badge]: https://gitlab.com/luthfifahlevi/sidoplak2/badges/develop/pipeline.svg
[coverage-badge]: https://gitlab.com/luthfifahlevi/sidoplak2/badges/develop/coverage.svg
[commits-gl]: https://gitlab.com/luthfifahlevi/sidoplak2/-/commits/develop