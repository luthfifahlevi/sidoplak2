from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

from daftar.models import DonorApplicationStatus
from .forms import CustomUserCreationForm, CustomUserLoginForm
from .models import CustomUser


def signUp(request):

    form = CustomUserCreationForm(request.POST or None)
    if request.POST and form.is_valid():
        form.save()

        email = request.POST['email']
        password = request.POST['password1']
        role = request.POST['role']
        user = authenticate(request, email=email, password=password, role=role)
        
        login(request, user)
        messages.success(request, f"Pendaftaran akun anda berhasil! Selamat datang {user.nickname()}.")
        return redirect("main:home")
            
    return render(request, 'registration/signup.html', {'form' : form,})
    

def auth_login(request):
    if request.user.is_authenticated:
        return redirect("main:home")
    
    # Prepopulate DonorApplicationStatus if there aren't any yet
    prepopulate_donor_application_status()

    form = CustomUserLoginForm(data=request.POST or None)
    if request.POST and form.is_valid():
        email = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, email=email, password=password)
        
        if user is not None:
            login(request, user)
            messages.success(request, f"Masuk akun berhasil. Selamat datang, {user.nickname()}.")
            return redirect("main:home")

    return render(request, 'registration/login.html', {'form' : form,})


def auth_logout(request):
    logout(request)
    return redirect('main:home')

def profile(request):
	return redirect('main:home')




''' Custom Helper Functions '''

def prepopulate_donor_application_status():
    status_exist = DonorApplicationStatus.objects.all()

    # If There is no DonorApplication objects, prepopulate db
    if not status_exist:
        status3 = DonorApplicationStatus(
            status = "3",
            message = "Terverifikasi",
            next = None
        )
        status3.save()

        status2 = DonorApplicationStatus(
            status = "2",
            message = "Menunggu Verifikasi",
            next = status3
        )
        status2.save()

        status1 = DonorApplicationStatus(
            status = "1",
            message = "Mencari Donor",
            next = status2
        )
        status1.save()

        status3.next = status1
        status3.save()