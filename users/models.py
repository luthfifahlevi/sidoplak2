from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import PermissionDenied
from artikel.models import Artikel

from .managers import CustomUserManager

USER_ROLE_CHOICES = [('PASIEN', 'PASIEN'), ('PENDONOR', 'PENDONOR'),]

class CustomUser(AbstractUser):
    username = None
    email = models.EmailField(_('email address'), unique=True)
    role = models.CharField(max_length = 255, choices = USER_ROLE_CHOICES, null=True, blank=True)
    is_admin_sidoplak = models.BooleanField(default=False)
    
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def natural_key(self):
        return self.full_name

    def __str__(self):
        return self.email

    def nickname(self):
        idx = self.email.find("@")
        return self.email[:idx].capitalize()

    def is_admin(self):
    	if self.is_superuser or self.is_admin_sidoplak:
    		return True
    	else:
    		return False
            
    def createArtikel(self, judul, isi):
        if not self.is_admin():
            raise PermissionDenied
            
        artikel = Artikel(judul = judul, isi = isi, penulis = self)
        try:
            artikel.full_clean()
            artikel.save()
        
            return True
        except PermissionDenied as pd:
            raise pd
        return False
    
    @staticmethod
    def getAllArtikel():
        return Artikel.objects.all()