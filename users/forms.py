from django.contrib.auth.forms import UserCreationForm, UserChangeForm, AuthenticationForm
from django import forms
from .models import CustomUser, USER_ROLE_CHOICES


USER_ROLE_CHOICES_FORM = [('', '* -- Pilih Peran Anda -- *')] + USER_ROLE_CHOICES


class CustomUserCreationForm(UserCreationForm):
	role = forms.ChoiceField(required = True, choices=USER_ROLE_CHOICES_FORM, widget=forms.Select(attrs={'type' : 'text', 'class' : 'form-text-control'}))

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.fields['email'].widget.attrs.update({'placeholder':'Masukkan alamat email', 'class':'form-text-control', 'autocomplete': 'on'})
		self.fields['password1'].widget.attrs.update({'placeholder':'Masukkan kata sandi', 'class':'form-text-control'})
		self.fields['password2'].widget.attrs.update({'placeholder':'Ulangi kata sandi', 'class':'form-text-control', 'autocomplete': 'off'})

	class Meta(UserCreationForm):
		model = CustomUser
		fields = ('email',)

	def clean(self):
		data = self.cleaned_data

		email = data['email']
		if CustomUser.objects.filter(email=email).first():
			raise forms.ValidationError(f"'{email}' sudah pernah digunakan. Coba lagi dengan email yang berbeda.")

	def save(self):
		user = super().save(commit=False)
		user.role = self.cleaned_data['role']
		user.save()

		
class CustomUserChangeForm(UserChangeForm):

    class Meta:
    	model = CustomUser
    	fields = ('email',)


class CustomUserLoginForm(AuthenticationForm):
	username = forms.EmailField(label='', required=True, max_length=100, widget=forms.TextInput(attrs={'type' : 'email', 'placeholder':'Masukkan alamat email', 'class':'form-text-control', 'id':'email', 'autocomplete': 'on'}))

	class Meta:
		model = CustomUser
		fields = ('email',)
	
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.fields['password'].widget.attrs.update({'placeholder':'Masukkan kata sandi', 'class':'form-text-control', 'id':'password'})

