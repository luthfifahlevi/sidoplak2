$(document).ready(function() {

	document.getElementById("button-submit").disabled = true;
	$("#button-submit").addClass("button-disabled");

	$("input[type='email'], input[type='password'], #id_role").on("change input", function(){
	    checkIsFormFilledRegister();
	});

	function checkIsFormFilledRegister() {
		if ($("input[name='email']").val() != "" && $("input[name='password1']").val() != "" && $("input[name='password2']").val() != "" && $("#id_role option:selected").val() != "") {
	        document.getElementById("button-submit").disabled = false;
    		$("#button-submit").removeClass("button-disabled");
	    } else {
	        document.getElementById("button-submit").disabled = true;
	    	$("#button-submit").addClass("button-disabled");
	    }
	}
});