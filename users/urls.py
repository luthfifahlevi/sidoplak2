from django.urls import path
from . import views

app_name = 'users'

urlpatterns = [
    path('signup/', views.signUp, name='signup'),
    path('login/', views.auth_login, name='login'),
    path('logout/', views.auth_logout, name='logout'),
    path('profile/', views.profile, name='profile'),
]