function closeModal(){
    let modal = document.getElementsByClassName("modal")[0];
    modal.style.display = "none";
}

function openModal(){
    let modal = document.getElementsByClassName("modal")[0];
    modal.style.display = "block";
}

function goToMainArticlePage(){
    window.location.replace("/artikel");
}

function manageArticle(event, id, op){
    event.stopPropagation();
    event.preventDefault();
    document.getElementById("id-artikel").value = id;
    let managerForm = document.getElementById("manager-form");
    managerForm.action = op;
    managerForm.submit();
}