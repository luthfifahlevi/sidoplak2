from django.test import TestCase
from django.urls import reverse
from django.core.exceptions import PermissionDenied
from users.models import CustomUser
from artikel.models import Artikel

class MainUnitTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        CustomUser.objects.create_user(email = "admin@adm.in", password = "admin", is_admin_sidoplak = True)
        CustomUser.objects.create_user(email = "user@us.er", password = "user")

    def setUp(self):
        self.client.logout()
        self.admin = CustomUser.objects.get(email = "admin@adm.in")
        self.user = CustomUser.objects.get(email = "user@us.er")
        Artikel(judul = "Test Judul", isi = "Test Isi", penulis = self.admin).save()

    def test_all_users_can_read_all_articles_200(self):
        response = self.client.get(reverse('artikel:main'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'artikel/main.html')
        
        self.client.login(email = "user@us.er", password = "user")
        response = self.client.get(reverse('artikel:main'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'artikel/main.html')
        
        self.client.login(email = "admin@adm.in", password = "admin")
        response = self.client.get(reverse('artikel:main'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'artikel/main.html')
        
    def test_admins_can_create_article_200(self):
        self.client.login(email = "admin@adm.in", password = "admin")
        response = self.client.get(reverse('artikel:buat'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'artikel/buat.html')
        
        response = self.client.post(reverse('artikel:buat'), {'judul': "Test Judul", 'isi': "Test Isi"})
        self.assertRedirects(response, reverse('artikel:main'))
    
    def test_non_admins_cant_create_article_403(self):
        response = self.client.get(reverse('artikel:buat'))
        self.assertEqual(response.status_code, 403)
        
        self.client.login(email = "user@us.er", password = "user")
        response = self.client.get(reverse('artikel:buat'))
        self.assertEqual(response.status_code, 403)
        
    def test_non_admin_cant_be_a_writer_of_an_article(self):
        with self.assertRaises(PermissionDenied):
            artikel = Artikel(judul = "Tes Title", isi = "Tes Content", penulis = self.user)
            artikel.full_clean()
        
    def test_admins_can_edit_article(self):
        self.client.login(email = "admin@adm.in", password = "admin")
        response = self.client.post(reverse('artikel:edit'), {'id-artikel': 1})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'artikel/edit.html')
        
        response = self.client.post(reverse('artikel:edit'), {'id-artikel': 1, 'judul': "Judul Test", 'isi': "Isi Test"})
        self.assertRedirects(response, reverse('artikel:main'))
        
    def test_admins_cant_edit_article_without_post_data_400(self):
        self.client.login(email = "admin@adm.in", password = "admin")
        response = self.client.get(reverse('artikel:edit'))
        self.assertEqual(response.status_code, 400)
        
        response = self.client.get(reverse('artikel:edit'), {'id-artikel': 1, 'judul': "Judul Test", 'isi': "Isi Test"})
        self.assertEqual(response.status_code, 400)
    
    def test_non_admins_cant_edit_article_403(self):
        response = self.client.get(reverse('artikel:edit'))
        self.assertEqual(response.status_code, 403)
        
        self.client.login(email = "user@us.er", password = "user")
        response = self.client.get(reverse('artikel:edit'))
        self.assertEqual(response.status_code, 403)
        
    def test_admins_can_delete_article(self):
        self.client.login(email = "admin@adm.in", password = "admin")
        response = self.client.post(reverse('artikel:delete'), {'id-artikel': 1})
        self.assertRedirects(response, reverse('artikel:main'))
        
    def test_admins_cant_delete_article_without_post_data_400(self):
        self.client.login(email = "admin@adm.in", password = "admin")
        response = self.client.get(reverse('artikel:delete'))
        self.assertEqual(response.status_code, 400)
        
    def test_non_admins_cant_delete_article_403(self):
        response = self.client.get(reverse('artikel:delete'))
        self.assertEqual(response.status_code, 403)
        
        self.client.login(email = "user@us.er", password = "user")
        response = self.client.get(reverse('artikel:delete'))
        self.assertEqual(response.status_code, 403)
        