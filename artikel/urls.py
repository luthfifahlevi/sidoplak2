from django.urls import path
from . import views

app_name = 'artikel'

urlpatterns = [
	path('', views.main, name='main'),
	path('buat', views.buat, name='buat'),
	path('lihat', views.lihat, name='lihat'),
	path('edit', views.edit, name='edit'),
	path('delete', views.delete, name='delete'),
]