from django.db import models
from django.core.exceptions import PermissionDenied
from django.utils.translation import gettext_lazy as _
from tinymce.models import HTMLField

class Artikel(models.Model):
    judul = models.CharField(max_length = 100)
    tanggal = models.DateField(auto_now_add = True)
    isi = HTMLField()
    penulis = models.ForeignKey(to = 'users.CustomUser', on_delete = models.SET_NULL, null = True)
    
    def clean(self):
        if self.penulis and not self.penulis.is_admin():
            raise PermissionDenied(_("Non-admin can't create an article"))    
    
    def deleteArtikel(self):
        try:
            super().delete()
            return True
        except:
            return False
        return False
            
    def editArtikel(self, judul, isi):
        try:
            self.judul = judul
            self.isi = isi
            self.full_clean()
            super().save()
            return True
        except:
            return False
        return False