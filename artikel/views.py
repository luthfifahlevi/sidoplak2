from django.shortcuts import render, redirect
from django.http import HttpResponseServerError
from django.core.exceptions import PermissionDenied, BadRequest
from .forms import ArtikelForm
from .models import Artikel
from users.models import CustomUser

def main(request):  
    all_artikel = CustomUser.getAllArtikel().order_by("-tanggal", "-id")
    is_admin = request.user.is_authenticated and request.user.is_admin()
    return render(request, 'artikel/main.html', {'all_artikel': all_artikel, 'is_admin': is_admin})

def buat(request):
    if not (request.user.is_authenticated and request.user.is_admin()):
        raise PermissionDenied
    
    form = ArtikelForm(request.POST or None)
    if request.POST and form.is_valid():
        judul = form.cleaned_data["judul"]
        isi = form.cleaned_data["isi"]
        penulis = CustomUser.objects.get(email = request.user.email)
        
        success = penulis.createArtikel(judul = judul, isi = isi)
        if success:
            return redirect("artikel:main")
        else:
            return HttpResponseServerError()
    
    return render(request, 'artikel/buat.html', {'form': form})
    
def lihat(request):
    if "id-artikel" not in request.POST:
        raise BadRequest
    
    id_artikel = int(request.POST["id-artikel"])
    artikel = Artikel.objects.get(id = id_artikel)
    
    return render(request, 'artikel/lihat.html', {'artikel': artikel})
    
def edit(request):
    if not (request.user.is_authenticated and request.user.is_admin()):
        raise PermissionDenied
        
    if "id-artikel" not in request.POST:
        raise BadRequest
    
    id_artikel = int(request.POST["id-artikel"])
    artikel = Artikel.objects.get(id = id_artikel)
    
    form = ArtikelForm(request.POST or None)
    if request.POST and form.is_valid():
        judul = form.cleaned_data["judul"]
        isi = form.cleaned_data["isi"]
        
        try:
            success = artikel.editArtikel(judul = judul, isi = isi)
            if success:
                return redirect("artikel:main")
            else:
                return HttpResponseServerError()
        except ValidationError:
            raise PermissionDenied
    else:
        form = ArtikelForm(initial = {'judul': artikel.judul, 'isi': artikel.isi})
    
    return render(request, 'artikel/edit.html', {'form': form, 'artikel': artikel})
    
def delete(request):
    if not (request.user.is_authenticated and request.user.is_admin()):
        raise PermissionDenied
        
    if "id-artikel" not in request.POST:
        raise BadRequest
    
    id_artikel = int(request.POST["id-artikel"])
    artikel = Artikel.objects.get(id = id_artikel)
    success = artikel.deleteArtikel()
    if not success:
        return HttpResponseServerError()
    return redirect("artikel:main")