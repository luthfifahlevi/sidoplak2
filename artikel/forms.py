from django.forms import ModelForm
from .models import Artikel
from tinymce.widgets import TinyMCE
import os

class ArtikelForm(ModelForm):
    class Meta:
        model = Artikel
        fields = ['judul', 'isi']
        widgets = {
            'isi': TinyMCE(mce_attrs = {
                'content_css': "/static/artikel/style.css"
            })
        }