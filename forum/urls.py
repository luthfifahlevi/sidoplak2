from django.urls import path
from . import views

app_name = 'forum'

urlpatterns = [
    path('', views.listforum, name='listforum'),
    path('create/', views.create, name='create'),
    path('detail/<int:idx>/', views.detail, name='detail'),
    path('update/<int:idx>/', views.update, name='update'),
    path('delete/<int:idx>/', views.delete, name='delete'),
]