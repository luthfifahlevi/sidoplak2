from django.db import models
from users.models import CustomUser

class Forum(models.Model):
    judul = models.CharField(max_length = 50)
    deskripsi = models.TextField()
    penulis = models.ForeignKey(CustomUser, on_delete=models.CASCADE, null=True)
    waktu = models.DateTimeField(auto_now=True) #when save, it's generated

    # def __str__(self):
    #     return "" + str(self.judul) + " " + str(self.penulis) + " " + str(self.waktu)

