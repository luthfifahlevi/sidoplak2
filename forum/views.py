from django.shortcuts import render, redirect
from .forms import Forum_Form
from .models import Forum

def listforum(request):
    posts = Forum.objects.all().order_by("-waktu")
    return render(request, 'listforum.html', {'posts':posts, 'banyakPost':len(posts)})


def create(request):
    forum_form = Forum_Form(request.POST or None)

    if request.method == "POST":
        if forum_form.is_valid():
           forum_form.save(request.user)
        
        return redirect('forum:detail', idx=forum_form.instance.pk)
    return render(request, 'create.html', {'forum_form' : forum_form})

def detail(request, idx):
    post = Forum.objects.get(pk = idx)
    return render(request, 'detail.html', {"post":post})

def update(request, idx):
    post = Forum.objects.get(pk = idx)
    forum_form = Forum_Form(request.POST or None, instance=post)

    if request.method == "POST":
        if forum_form.is_valid():
            forum_form.save(request.user)

        return redirect('forum:detail', idx=idx)

    return render(request, 'update.html', {'forum_form' : forum_form, 'post_id':idx})


def delete(request, idx):
    Forum.objects.filter(id = idx).delete()
    return redirect('forum:listforum')
