from django.test import tag
from django.test import TestCase, LiveServerTestCase
from .models import Forum
from .forms import Forum_Form
from .apps import ForumConfig
from selenium import webdriver
from users.models import CustomUser
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.select import Select
import time


class UnitTest(TestCase):
    def test_app(self):
        self.assertEqual(ForumConfig.name, "forum")

    def test_form(self):
        form_data = {'judul': 'Luthfi', 'deskripsi': 'just test'}
        form =  Forum_Form(data=form_data)
        self.assertTrue(form.is_valid())
        form_data={}
        form =  Forum_Form(data=form_data)
        self.assertFalse(form.is_valid())

    # @classmethod
    # def setUpTestData(cls):
    #     CustomUser.objects.create_user(email = "admin@test.com", password = "adminrpl123", is_admin_sidoplak = True)
    #     CustomUser.objects.create_user(email = "test.pendonor@test.com", password = "testpendonorrpl123")

    # def setUp(self):
    #     self.client.logout()
    #     self.admin = CustomUser.objects.get(email = "admin@test.com")
    #     self.user = CustomUser.objects.get(email = "test.pendonor@test.com")
    #     Forum(judul = "Test Judul", deskripsi = "Test Isi", penulis = self.admin).save()
    
    # def test_nonLogin_read(self):
    #     response = self.client.get('/forum/')
    #     self.assertEqual(response.status_code, 200)
    #     self.assertTemplateUsed(response, 'listforum.html')
    
    # def test_admin_CRUD(self):
    #     self.client.login(email = "admin@test.com", password = "adminrpl123")

    #     # Read
    #     response = self.client.get('/forum/')
    #     self.assertEqual(response.status_code, 200)
    #     self.assertTemplateUsed(response, 'listforum.html')

    #     # Create
    #     response = self.client.get('/forum/create/')
    #     self.assertEqual(response.status_code, 200)
    #     self.assertTemplateUsed(response, 'create.html')
    #     response = self.client.post('/forum/create/', {'judul': "test judul", 'deskripsi': "test deskripsi"})
    #     self.assertRedirects(response, 'forum/detail/' + str(response.pk)+"/")

    #     # Edit
    #     response = self.client.get('/forum/update/' + str(response.pk)+"/")
    #     self.assertEqual(response.status_code, 200)
    #     self.assertTemplateUsed(response, 'update.html')
    #     response = self.client.post('/forum/update/' + str(response.pk)+"/", {'judul': "test judul", 'deskripsi': "test deskripsi"})

    #     response = self.client.post('forum/delete'+ str(response.pk)+"/", {'idx': response.pk})
    #     self.assertRedirects(response, '/forum/')

@tag('functional')
class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument("--window-size=1920,1080")
        chrome_options.add_argument('--dns-prefectch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path='./chromedriver.log'
        service_args=['--verbose']
        # self.browser = webdriver.Chrome(executable_path='./chromedriver', chrome_options = chrome_options)
        self.browser = webdriver.Chrome(executable_path='/usr/bin/chromedriver', chrome_options = chrome_options)
# 

    def tearDown(self):
        self.browser.quit()
        super().tearDown()
    
    def test_flow(self):
        selenium = self.browser 
        # selenium.get(('%s%s' % ('http://127.0.0.1:8000', '/')))
        # print(self.live_server_url)
        selenium.get(('%s%s' % (self.live_server_url, '/')))
        time.sleep(1)
        self.assertIn("Selamat Datang", selenium.page_source)
        
        try:
            # login account
            daftar_button = selenium.find_element_by_xpath('//a[@href="'+'/akun/login/'+'"]')
            daftar_button.click()
            # time.sleep(1)
            self.assertIn("Masuk", selenium.page_source)
        
            email = selenium.find_element_by_name('username')
            email.send_keys("test.pendonor@test.com")
            kataSandi = selenium.find_element_by_name('password')
            kataSandi.send_keys("testpendonorrpl123")
            submit = selenium.find_element_by_xpath('//*[@id="button-submit"]')
            submit.click()
            time.sleep(1)
            self.assertIn("Test.pendonor", selenium.page_source)
        except:
        # sign up
            # selenium.get(('%s%s' % ('http://127.0.0.1:8000', '/')))
            selenium.get(('%s%s' % (self.live_server_url, '/')))
            time.sleep(1)
            daftar_button = selenium.find_element_by_xpath('//a[@href="'+'/akun/signup/'+'"]')
            daftar_button.click()
            # time.sleep(1)
            self.assertIn("Daftar", selenium.page_source)
        
            role = selenium.find_element_by_xpath('//*[@id="id_role"]')
            role = Select(role)
            role.select_by_visible_text("PENDONOR")
            email = selenium.find_element_by_name('email')
            email.send_keys("test.pendonor@test.com")
            kataSandi = selenium.find_element_by_name('password1')
            kataSandi.send_keys("testpendonorrpl123")
            ulangKataSandi = selenium.find_element_by_name('password2')
            ulangKataSandi.send_keys("testpendonorrpl123")
            submit = selenium.find_element_by_xpath('//*[@id="button-submit"]')
            submit.click()
            time.sleep(1)
            self.assertIn("Test.pendonor", selenium.page_source)
        # ---

        forum_button = selenium.find_element_by_xpath('//a[@href="'+'/forum/'+'"]')
        forum_button.click()
        # time.sleep(1)
        self.assertIn("Buat Post", selenium.page_source)

        # create
        buatPost = selenium.find_element_by_xpath('//a[@href="'+'/forum/create/'+'"]')
        buatPost.click()
        # time.sleep(1)
        self.assertIn("Buat Postinganmu!", selenium.page_source)

        judul = selenium.find_element_by_name('judul')
        judul.send_keys("test judul")
        deskripsi = selenium.find_element_by_name('deskripsi')
        deskripsi.send_keys("test deskripsi")
        kirim = selenium.find_element_by_xpath('/html/body/div/div/div/form/div[3]/button')
        kirim.click()
        time.sleep(1)
        self.assertIn("test judul", selenium.page_source)
        self.assertIn("test deskripsi", selenium.page_source)

        # back to list forum
        forum_button = selenium.find_element_by_xpath('//a[@href="'+'/forum/'+'"]')
        forum_button.click()
        # time.sleep(1)
        self.assertIn("Buat Post", selenium.page_source)

        # update
        ubah = selenium.find_element_by_name('ubah')
        ubah.click()
        # time.sleep(1)
        self.assertIn("test deskripsi", selenium.page_source)

        judulUbah = selenium.find_element_by_name('judul')
        judulUbah.send_keys("test ubah")
        kirimUbah = selenium.find_element_by_xpath('/html/body/div/div/div/form/div[3]/button')
        kirimUbah.click()
        time.sleep(1)
        self.assertIn("test ubah", selenium.page_source)

        # back to list forum
        forum_button = selenium.find_element_by_xpath('//a[@href="'+'/forum/'+'"]')
        forum_button.click()
        time.sleep(1)
        self.assertIn("Buat Post", selenium.page_source)

        # detail        
        judulList = selenium.find_element_by_class_name('judul')
        judulList.click()
        # time.sleep(1)
        self.assertIn("test deskripsi", selenium.page_source)
        
        # back to list forum
        forum_button = selenium.find_element_by_xpath('//a[@href="'+'/forum/'+'"]')
        forum_button.click()
        time.sleep(1)
        self.assertIn("Buat Post", selenium.page_source)

        # delete
        delete = selenium.find_element_by_name('hapus')
        delete.click()
        time.sleep(1)
        yakin = selenium.find_element_by_id('hapus-btn')
        yakin.click()
        # time.sleep(1)
        self.assertNotIn("test ubah", selenium.page_source)
