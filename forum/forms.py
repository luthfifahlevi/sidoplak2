from django import forms
from .models import Forum
# Create your views here.

class Forum_Form(forms.ModelForm):
    class Meta:
        model = Forum
        fields = ['judul', 'deskripsi']
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
        })
        
    def save(self, user):
	    forum = super().save(commit=False)
	    forum.penulis = user
	    return forum.save()
