from django import forms

from adminprofilpasienpendonor.constants import get_address_province_as_form_choices
from users.models import USER_ROLE_CHOICES


ADDRESS_PROVINCE_FILTER_CHOICES = get_address_province_as_form_choices("--Provinsi--")
BLOOD_TYPE_FILTER_CHOICES = (('', "--Goldar--"), ('A+', "A+"), ('A-', "A-"), ('B+', "B+"), ('B-', "B-"), ('O+', "O+"), ('O-', "O-"), ('AB+', "AB+"), ('AB-', "AB-"),)
SHOW_FILTER_CHOICES = (('10', '10 entri'), ('15', '15 entri'), ('20', '20 entri'), ('50', '50 entri'), ('semua', 'Semua entri'),)
IS_DONE_CHOICES = (("False", 'Belum Terverifikasi'), ("True", 'Sudah Terverifikasi'))


class MatchListFiltersForm(forms.Form):
	show = forms.ChoiceField(required = False, choices=SHOW_FILTER_CHOICES, widget=forms.Select(attrs={'type' : 'text', 'class' : 'form-text-control'}))
	fil_prov = forms.ChoiceField(required = False, choices=ADDRESS_PROVINCE_FILTER_CHOICES, widget=forms.Select(attrs={'type' : 'text', 'class' : 'form-text-control'}))
	fil_goldar = forms.ChoiceField(required = False, choices=BLOOD_TYPE_FILTER_CHOICES, widget=forms.Select(attrs={'type' : 'text', 'class' : 'form-text-control'}))
	fil_role = forms.ChoiceField(required = False, choices=USER_ROLE_CHOICES, widget=forms.Select(attrs={'type' : 'text', 'class' : 'form-text-control'}))
	search_patient = forms.CharField(required = False, max_length=255, widget=forms.TextInput(attrs={'type' : 'text', 'placeholder' : 'Cari nama pasien', 'class' : 'form-text-control', 'autocomplete' : 'off'}))
	search_donor = forms.CharField(required = False, max_length=255, widget=forms.TextInput(attrs={'type' : 'text', 'placeholder' : 'Cari nama pendonor', 'class' : 'form-text-control', 'autocomplete' : 'off'}))
	is_done = forms.ChoiceField(required = False, choices=IS_DONE_CHOICES, widget=forms.Select(attrs={'type' : 'text', 'class' : 'form-text-control'}))