from django.apps import AppConfig


class AdminverifikasipasanganConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'adminverifikasipasangan'
