from django.urls import path
from . import views

app_name = 'adminverifikasipasangan'

urlpatterns = [
	path('', views.list_match, name='list_match'),
	path('terima/<str:donor_app_pk>/', views.accept_match, name='accept_match'),
	path('tolak/<str:donor_app_pk>/', views.reject_match, name='reject_match'),

	path('download/pendonor/<int:donor_app_pk>/', views.download_donor_files, name='download_donor_files'),
	path('download/pasien/<int:donor_app_pk>/', views.download_patient_files, name='download_patient_files'),
]