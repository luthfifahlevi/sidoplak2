$(document).ready(function() {

	var href = window.location.href;
	var indexHostName = href.search("admin-sidoplak");
	var hostname = href.slice(0, indexHostName-1)

	$(".matches-patient-card, .matches-donor-card").on("click", function() {
		openInNewTab($(this).attr("href"));
	});

	$(".matches a").on("click", function(e) {
		e.stopPropagation();
	})

	function openInNewTab(url) {
		var paramWhere = url.search("pk=");
		var uri = url.slice(0, paramWhere);
		var param = url.slice(paramWhere + 3);

		var url_result = hostname + uri + "pk=" + encodeURIComponent(param);
		window.open(url_result, '_self').focus();
	}

});