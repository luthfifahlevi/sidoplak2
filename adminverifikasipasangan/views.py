from django.shortcuts import render, redirect
from django.http import Http404, HttpResponse
from django.conf import settings
from django.contrib import messages

from daftar.models import DonorApplication
from .forms import MatchListFiltersForm

import zipfile
import io
import os
import urllib.parse


def list_match(request):
	if not request.user.is_admin():
		raise Http404


	#GET paramters init
	page = 1
	show = 10
	fil_prov = None
	fil_goldar = None
	search_patient = None
	search_donor = None
	is_done = "False"

	url_param = "" #Create URL get parameter for page viewing

	if request.GET:
		if "page" in request.GET:
			if request.GET['page'].isdigit():
				page = int(request.GET['page'])
		if "is_done" in request.GET:
			is_done = request.GET['is_done']
			url_param += f"is_done={is_done}&"
		if "show" in request.GET:
			if request.GET['show'].isdigit():
				show = int(request.GET['show'])
				url_param += f"show={str(show)}&"
			elif request.GET['show'] == "semua":
				if is_done == "True":
					show = DonorApplication.objects.exclude(donor__isnull=True).filter(status__status=3).count()
				else:
					show = DonorApplication.objects.exclude(donor__isnull=True).exclude(status__status=3).count()
				url_param += f"show=semua&"
		if "fil_prov" in request.GET:
			fil_prov = request.GET['fil_prov']
			url_param += f"fil_prov={fil_prov}&"
		if "fil_goldar" in request.GET:
			fil_goldar = request.GET['fil_goldar']
			url_param += f"fil_goldar={fil_goldar}&"
		if "search_patient" in request.GET:
			search_patient = request.GET['search_patient']
			url_param += f"search_patient={search_patient}&"
		if "search_donor" in request.GET:
			search_donor = request.GET['search_donor']
			url_param += f"search_donor={search_donor}&"
	elif request.POST:
		page = 1
		if "is_done" in request.POST:
			if request.POST['is_done'] != "":
				is_done = request.POST['is_done']
				url_param += f"is_done={is_done}&"
		if "show" in request.POST:
			if request.POST['show'] != "" and request.POST['show'].isdigit():
				show = int(request.POST['show'])
				url_param += f"show={str(show)}&"
			elif request.POST['show'] == "semua":
				if is_done == "True":
					show = DonorApplication.objects.exclude(donor__isnull=True).filter(status__status=3).count()
				else:
					show = DonorApplication.objects.exclude(donor__isnull=True).exclude(status__status=3).count()
				url_param += f"show=semua&"
		if "fil_prov" in request.POST:
			if request.POST['fil_prov'] != "":
				fil_prov = request.POST['fil_prov']
				url_param += f"fil_prov={fil_prov}&"
		if "fil_goldar" in request.POST:
			if request.POST['fil_goldar'] != "":
				fil_goldar = request.POST['fil_goldar']
				url_param += f"fil_goldar={fil_goldar}&"
		if "search_patient" in request.POST:
			if request.POST['search_patient'] != "":
				search_patient = request.POST['search_patient']
				url_param += f"search_patient={search_patient}&"
		if "search_donor" in request.POST:
			if request.POST['search_donor'] != "":
				search_donor = request.POST['search_donor']
				url_param += f"search_donor={search_donor}&"

	url_param = url_param.replace("+", "%2B") #URL encoding

	if show <= 0:
		show = 1

	#set filter for query
	if is_done == "True":
		donor_applications = DonorApplication.objects.exclude(donor__isnull=True).filter(status__status=3)
	else:
		donor_applications = DonorApplication.objects.exclude(donor__isnull=True).exclude(status__status=3)
	
	if fil_prov is not None:
		donor_applications = donor_applications.filter(patient__address_province__iexact=fil_prov)
	if fil_goldar is not None:
		goldar = fil_goldar[:-1]
		rhesus = fil_goldar[-1:]
		donor_applications = donor_applications.filter(patient__medical_blood_type=goldar).filter(patient__medical_rhesus=rhesus)
	if search_patient is not None:
		donor_applications = donor_applications.filter(patient__full_name__icontains=search_patient)
	if search_donor is not None:
		donor_applications = donor_applications.filter(donor__full_name__icontains=search_donor)

	#number of pages
	num_of_donor = donor_applications.count()
	num_of_pages = int(num_of_donor / show) + (num_of_donor % show > 0)

	if (page <= 0 or page > num_of_pages) and num_of_pages != 0:
		raise Http404

	list_page_number_bar = []
	for i in range(-1, 2):
		page_number_bar = page + i
		if page_number_bar > 0 and page_number_bar <= num_of_pages:
			list_page_number_bar.append(page_number_bar)

	is_there_more_pages_left = False
	is_there_more_pages_right = False
	if (page - 2) > 0:
		is_there_more_pages_left = True
	if (page + 2) <= num_of_pages:
		is_there_more_pages_right = True

	#get PatientRequest objects
	idx_start = show * (page-1)
	idx_end = show * page
	donor_applications = donor_applications.select_related('patient').select_related('patient__user').select_related('donor').select_related('status').order_by("-timestamp_created")[idx_start:idx_end]


	#Filter Form
	form = MatchListFiltersForm(request.POST or None)
	if is_done == "True":
		form.fields['is_done'].initial = "True"
	else:
		form.fields['is_done'].initial = "False"
	if show == num_of_donor:
		form.fields['show'].initial = "semua"
	else:
		form.fields['show'].initial = show
	if fil_prov is not None:
		form.fields['fil_prov'].initial = fil_prov
	if fil_goldar is not None:
		form.fields['fil_goldar'].initial = fil_goldar
	if search_patient is not None:
		form.fields['search_patient'].initial = search_patient
	if search_donor is not None:
		form.fields['search_donor'].initial = search_donor

	context = {
		'form' : form,
		'donor_applications' : donor_applications,
		'list_page_number_bar' : list_page_number_bar,
		'page' : page,
		'num_of_pages' : num_of_pages,
		'url_param' : url_param,
		'is_there_more_pages_left' : is_there_more_pages_left,
		'is_there_more_pages_right' : is_there_more_pages_right,
	}

	return render(request, "adminverifikasipasangan/list_match.html", context)



def accept_match(request, donor_app_pk):
	if not request.user.is_admin():
		raise Http404

	donor_application = DonorApplication.objects.filter(pk=donor_app_pk)
	if donor_application:
		donor_application = donor_application.first()

		donor_application.donor.has_donor = True
		donor_application.donor.save()

		donor_application.patient.is_requesting = False
		donor_application.patient.save()

		donor_application.status = donor_application.status.next
		donor_application.save()

		messages.success(request, f"Pasangan (ID: {donor_app_pk}) berhasil diverifikasi")

		return redirect("adminverifikasipasangan:list_match")

	raise Http404


def reject_match(request, donor_app_pk):
	if not request.user.is_admin():
		raise Http404

	donor_application = DonorApplication.objects.filter(pk=donor_app_pk)
	if donor_application:
		donor_application = donor_application.first()

		donor_application.donor = None

		donor_application.status = donor_application.status.prev
		donor_application.save()

		messages.success(request, f"Pasangan (ID: {donor_app_pk}) berhasil ditolak")

		return redirect("adminverifikasipasangan:list_match")

	raise Http404


def download_patient_files(request, donor_app_pk):
	if not request.user.is_admin():
		raise Http404

	donor_application = DonorApplication.objects.filter(pk=donor_app_pk)
	if donor_application:
		donor_application = donor_application.first()
		if donor_application.file != "" and donor_application.file is not None:
			file_name = donor_application.file.url.replace("/media/", "", 1)

			if os.path.isfile(settings.MEDIA_ROOT / file_name):
				zip_subdir = f"berkas-pasien-permohononan-{donor_app_pk}"
				zip_filename = "%s.zip" % zip_subdir
				return download_as_zip([file_name], zip_subdir, zip_filename)
		
		return HttpResponse("FILE NOT FOUND: \n\nHeroku tidak mendukung unggahan file lama karena akan dihapus secara berkala pada filesystemnya.")

	raise Http404


def download_donor_files(request, donor_app_pk):
	if not request.user.is_admin():
		raise Http404

	donor_application = DonorApplication.objects.filter(pk=donor_app_pk)
	if donor_application:
		donor_application = donor_application.first()
		file_pcr_positive_obj = donor_application.donor.file_pcr_positive
		file_pcr_negative_obj = donor_application.donor.file_pcr_negative

		files_list = []
		if file_pcr_positive_obj != "" and file_pcr_positive_obj is not None:
			file_name_pcr_positive = file_pcr_positive_obj.url.replace("/media/", "", 1)
			if os.path.isfile(settings.MEDIA_ROOT / file_name_pcr_positive):
				files_list.append(file_name_pcr_positive)
		if file_pcr_negative_obj != "" and file_pcr_negative_obj is not None:
			file_name_pcr_negative = file_pcr_negative_obj.url.replace("/media/", "", 1)
			if os.path.isfile(settings.MEDIA_ROOT / file_name_pcr_negative):
				files_list.append(file_name_pcr_negative)

		if len(files_list) > 0:
			zip_subdir = f"berkas-pendonor-permohononan-{donor_app_pk}"
			zip_filename = "%s.zip" % zip_subdir
			return download_as_zip(files_list, zip_subdir, zip_filename)
		
		return HttpResponse("FILE NOT FOUND: \n\nHeroku tidak mendukung unggahan file lama karena akan dihapus secara berkala pada filesystemnya.")

	raise Http404



''' CUSTOM FUNCTION '''

def download_as_zip(filenames, zip_subdir, zip_filename):
	# Open StringIO to grab in-memory ZIP contents
	s = io.BytesIO()

	# The zip compressor
	zf = zipfile.ZipFile(s, "w")

	media_root = settings.MEDIA_ROOT
	for fpath in filenames:
		fpath = media_root / fpath
		# Calculate path for file in zip
		fdir, fname = os.path.split(fpath)
		zip_path = os.path.join(zip_subdir, fname)

		# Add file, at correct path
		zf.write(fpath, zip_path)

	# Must close zip for all contents to be written
	zf.close()

	# Grab ZIP file from in-memory, make response with correct MIME-type
	response = HttpResponse(s.getvalue(), content_type = "application/x-zip-compressed")
	response['Content-Disposition'] = 'attachment; filename=%s' % zip_filename

	return response