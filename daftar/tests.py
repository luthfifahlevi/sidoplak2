from datetime import date, datetime
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from django.urls import reverse
from daftar.models import DonorApplication, DonorApplicationStatus, Pasien, Pendonor
from users.models import CustomUser

class MainUnitTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        CustomUser.objects.create_user(email="pasien@test.com", password="pasien", role='PASIEN')
        CustomUser.objects.create_user(email="pendonor@test.com", password="pendonor", role='PENDONOR')
        DonorApplicationStatus(status="1",message="Mencari Donor").save()
        DonorApplicationStatus(status="2",message="Menunggu Verifikasi").save()
        DonorApplicationStatus(status="3",message="Terverifikasi").save()

    def setUp(self):
        self.pasien = CustomUser.objects.get(email = "pasien@test.com")
        self.pendonor = CustomUser.objects.get(email = "pendonor@test.com")

    # PASIEN
    def test_pasien_create_fail_auth(self):
        self.client.login(email="pendonor@test.com", password="pendonor")
        response = self.client.get(reverse('daftar:daftarPasien'))
        self.assertTemplateUsed(response, '404.html')

    def test_pasien_create_fail_file(self):
        self.client.login(email="pasien@test.com", password="pasien")
        response = self.client.get(reverse('daftar:daftar'))
        self.assertEqual(response.status_code, 302)

        file = SimpleUploadedFile("test.docx", b"content", content_type="docx")
        response = self.client.post(reverse('daftar:daftarPasien'), {'user': str(self.pasien.id), 'is_requesting': 'on', 'full_name': 'Test', 'nik': '1', 'nohp': '1', 'gender': 'LAKI-LAKI', 'place_of_birth': 'A', 'date_of_birth': date(2000, 1, 1), 'profession': 'BUMN', 'address': 'A', 'address_province': 'ACEH', 'address_regency': 'KABUPATEN SIMEULEU', 'address_district': 'TEUPAH SELATAN', 'address_village': 'LATIUNG', 'medical_blood_type': 'A', 'medical_rhesus': '+', 'medical_weight': 1, 'medical_height': 1, 'agree_to_the_terms_and_conditions': 'on', 'timestamp_last_request': datetime(2020, 12, 12, 1, 1, 1), 'file': file})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'formPasien.html')

    def test_pasien_create_fail_form(self):
        self.client.login(email="pasien@test.com", password="pasien")
        response = self.client.get(reverse('daftar:daftar'))
        self.assertEqual(response.status_code, 302)

        file = SimpleUploadedFile("test.pdf", b"content", content_type="pdf")
        response = self.client.post(reverse('daftar:daftarPasien'), {'user': str(self.pasien.id), 'is_requesting': 'on', 'nik': '1', 'nohp': '1', 'gender': 'LAKI-LAKI', 'place_of_birth': 'A', 'date_of_birth': date(2000, 1, 1), 'profession': 'BUMN', 'address': 'A', 'address_province': 'ACEH', 'address_regency': 'KABUPATEN SIMEULEU', 'address_district': 'TEUPAH SELATAN', 'address_village': 'LATIUNG', 'medical_blood_type': 'A', 'medical_rhesus': '+', 'medical_weight': 1, 'medical_height': 1, 'agree_to_the_terms_and_conditions': 'on', 'timestamp_last_request': datetime(2020, 12, 12, 1, 1, 1), 'file': file})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'formPasien.html')

    def test_pasien_create_success(self):
        self.client.login(email="pasien@test.com", password="pasien")
        response = self.client.get(reverse('daftar:daftar'))
        self.assertEqual(response.status_code, 302)

        file = SimpleUploadedFile("test.pdf", b"content", content_type="pdf")
        response = self.client.post(reverse('daftar:daftarPasien'), {'user': str(self.pasien.id), 'is_requesting': 'on', 'full_name': 'Test', 'nik': '1', 'nohp': '1', 'gender': 'LAKI-LAKI', 'place_of_birth': 'A', 'date_of_birth': date(2000, 1, 1), 'profession': 'BUMN', 'address': 'A', 'address_province': 'ACEH', 'address_regency': 'KABUPATEN SIMEULEU', 'address_district': 'TEUPAH SELATAN', 'address_village': 'LATIUNG', 'medical_blood_type': 'A', 'medical_rhesus': '+', 'medical_weight': 1, 'medical_height': 1, 'agree_to_the_terms_and_conditions': 'on', 'timestamp_last_request': datetime(2020, 12, 12, 1, 1, 1), 'file': file})
        self.assertEqual(response.status_code, 302)

    def test_pasien_create_success_update(self):
        self.client.login(email="pasien@test.com", password="pasien")
        response = self.client.get(reverse('daftar:daftarPasien'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'formPasien.html')

        file = SimpleUploadedFile("test.pdf", b"content", content_type="pdf")
        response = self.client.post(reverse('daftar:daftarPasien'), {'user': str(self.pasien.id), 'is_requesting': 'on', 'full_name': 'Test', 'nik': '1', 'nohp': '1', 'gender': 'LAKI-LAKI', 'place_of_birth': 'A', 'date_of_birth': date(2000, 1, 1), 'profession': 'BUMN', 'address': 'A', 'address_province': 'ACEH', 'address_regency': 'KABUPATEN SIMEULEU', 'address_district': 'TEUPAH SELATAN', 'address_village': 'LATIUNG', 'medical_blood_type': 'A', 'medical_rhesus': '+', 'medical_weight': 1, 'medical_height': 1, 'agree_to_the_terms_and_conditions': 'on', 'timestamp_last_request': datetime(2020, 12, 12, 1, 1, 1), 'file': file})
        self.assertEqual(response.status_code, 302)

        response = self.client.get(reverse('daftar:daftarPasien'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'formPasien.html')

        file = SimpleUploadedFile("test.pdf", b"content", content_type="pdf")
        response = self.client.post(reverse('daftar:daftarPasien'), {'user': str(self.pasien.id), 'is_requesting': 'on', 'full_name': 'Test', 'nik': '12', 'nohp': '1', 'gender': 'LAKI-LAKI', 'place_of_birth': 'A', 'date_of_birth': date(2000, 1, 1), 'profession': 'BUMN', 'address': 'A', 'address_province': 'ACEH', 'address_regency': 'KABUPATEN SIMEULEU', 'address_district': 'TEUPAH SELATAN', 'address_village': 'LATIUNG', 'medical_blood_type': 'A', 'medical_rhesus': '+', 'medical_weight': 1, 'medical_height': 1, 'agree_to_the_terms_and_conditions': 'on', 'timestamp_last_request': datetime(2020, 12, 12, 1, 1, 1), 'file': file})
        self.assertEqual(response.status_code, 302)

    # PENDONOR
    def test_pendonor_create_fail_auth(self):
        self.client.login(email="pasien@test.com", password="pasien")
        response = self.client.get(reverse('daftar:daftarPendonor'))
        self.assertTemplateUsed(response, '404.html')

    def test_pendonor_create_fail_file(self):
        self.client.login(email="pendonor@test.com", password="pendonor")
        response = self.client.get(reverse('daftar:daftar'))
        self.assertEqual(response.status_code, 302)

        file1 = SimpleUploadedFile("test.docx", b"content", content_type="docx")
        file2 = SimpleUploadedFile("test.docx", b"content", content_type="docx")
        response = self.client.post(reverse('daftar:daftarPendonor'), {'user': str(self.pendonor.id), 'is_requesting': 'on', 'full_name': 'Test', 'nik': '1', 'nohp': '1', 'gender': 'LAKI-LAKI', 'place_of_birth': 'A', 'date_of_birth': date(2000, 1, 1), 'profession': 'BUMN', 'address': 'A', 'address_province': 'ACEH', 'address_regency': 'KABUPATEN SIMEULEU', 'address_district': 'TEUPAH SELATAN', 'address_village': 'LATIUNG', 'medical_blood_type': 'A', 'medical_rhesus': '+', 'medical_weight': 1, 'medical_height': 1, 'agree_to_the_terms_and_conditions': 'on', 'timestamp_last_request': datetime(2020, 12, 12, 1, 1, 1), 'file_pcr_positive': file1, 'file_pcr_negative': file2})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'formPendonor.html')

    def test_pendonor_create_fail_form(self):
        self.client.login(email="pendonor@test.com", password="pendonor")
        response = self.client.get(reverse('daftar:daftarPendonor'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'formPendonor.html')

        file1 = SimpleUploadedFile("test.pdf", b"content", content_type="pdf")
        file2 = SimpleUploadedFile("test.pdf", b"content", content_type="pdf")
        response = self.client.post(reverse('daftar:daftarPendonor'), {'pdf': str(self.pendonor.id), 'is_requesting': 'on', 'nik': '1', 'nohp': '1', 'gender': 'LAKI-LAKI', 'place_of_birth': 'A', 'date_of_birth': date(2000, 1, 1), 'profession': 'BUMN', 'address': 'A', 'address_province': 'ACEH', 'address_regency': 'KABUPATEN SIMEULEU', 'address_district': 'TEUPAH SELATAN', 'address_village': 'LATIUNG', 'medical_blood_type': 'A', 'medical_rhesus': '+', 'medical_weight': 1, 'medical_height': 1, 'agree_to_the_terms_and_conditions': 'on', 'timestamp_last_request': datetime(2020, 12, 12, 1, 1, 1), 'file_pcr_positive': file1, 'file_pcr_negative': file2})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'formPendonor.html')

    def test_pendonor_create_success(self):
        self.client.login(email="pasien@test.com", password="pasien")
        response = self.client.get(reverse('daftar:daftar'))
        self.assertEqual(response.status_code, 302)

        file = SimpleUploadedFile("test.pdf", b"content", content_type="pdf")
        response = self.client.post(reverse('daftar:daftarPasien'), {'user': str(self.pasien.id), 'is_requesting': 'on', 'full_name': 'Test', 'nik': '1', 'nohp': '1', 'gender': 'LAKI-LAKI', 'place_of_birth': 'A', 'date_of_birth': date(2000, 1, 1), 'profession': 'BUMN', 'address': 'A', 'address_province': 'ACEH', 'address_regency': 'KABUPATEN SIMEULEU', 'address_district': 'TEUPAH SELATAN', 'address_village': 'LATIUNG', 'medical_blood_type': 'A', 'medical_rhesus': '+', 'medical_weight': 1, 'medical_height': 1, 'agree_to_the_terms_and_conditions': 'on', 'timestamp_last_request': datetime(2020, 12, 12, 1, 1, 1), 'file': file})
        self.assertEqual(response.status_code, 302)

        self.client.login(email="pendonor@test.com", password="pendonor")
        response = self.client.get(reverse('daftar:daftarPendonor'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'formPendonor.html')

        file1 = SimpleUploadedFile("test.pdf", b"content", content_type="pdf")
        file2 = SimpleUploadedFile("test.pdf", b"content", content_type="pdf")
        response = self.client.post("%s?wid=%s" % (reverse('daftar:daftarPendonor'), 'pasien@test.com'), {'user': str(self.pendonor.id), 'is_requesting': 'on', 'full_name': 'Test', 'nik': '1', 'nohp': '1', 'gender': 'LAKI-LAKI', 'place_of_birth': 'A', 'date_of_birth': date(2000, 1, 1), 'profession': 'BUMN', 'address': 'A', 'address_province': 'ACEH', 'address_regency': 'KABUPATEN SIMEULEU', 'address_district': 'TEUPAH SELATAN', 'address_village': 'LATIUNG', 'medical_blood_type': 'A', 'medical_rhesus': '+', 'medical_weight': 1, 'medical_height': 1, 'agree_to_the_terms_and_conditions': 'on', 'timestamp_last_request': datetime(2020, 12, 12, 1, 1, 1), 'file_pcr_positive': file1, 'file_pcr_negative': file2, 'disease_history': 'A', 'date_pcr_positive': date(2000, 12, 12), 'date_pcr_negative': date(2000, 12, 1)})
        self.assertEqual(response.status_code, 302)

    def test_pendonor_create_success_update(self):
        self.client.login(email="pasien@test.com", password="pasien")
        response = self.client.get(reverse('daftar:daftar'))
        self.assertEqual(response.status_code, 302)

        file = SimpleUploadedFile("test.pdf", b"content", content_type="pdf")
        response = self.client.post(reverse('daftar:daftarPasien'), {'user': str(self.pasien.id), 'is_requesting': 'on', 'full_name': 'Test', 'nik': '1', 'nohp': '1', 'gender': 'LAKI-LAKI', 'place_of_birth': 'A', 'date_of_birth': date(2000, 1, 1), 'profession': 'BUMN', 'address': 'A', 'address_province': 'ACEH', 'address_regency': 'KABUPATEN SIMEULEU', 'address_district': 'TEUPAH SELATAN', 'address_village': 'LATIUNG', 'medical_blood_type': 'A', 'medical_rhesus': '+', 'medical_weight': 1, 'medical_height': 1, 'agree_to_the_terms_and_conditions': 'on', 'timestamp_last_request': datetime(2020, 12, 12, 1, 1, 1), 'file': file})
        self.assertEqual(response.status_code, 302)

        self.client.login(email="pendonor@test.com", password="pendonor")
        response = self.client.get(reverse('daftar:daftar'))
        self.assertEqual(response.status_code, 302)

        file1 = SimpleUploadedFile("test.pdf", b"content", content_type="pdf")
        file2 = SimpleUploadedFile("test.pdf", b"content", content_type="pdf")
        response = self.client.post("%s?wid=%s" % (reverse('daftar:daftarPendonor'), 'pasien@test.com'), {'user': str(self.pendonor.id), 'is_requesting': 'on', 'full_name': 'Test', 'nik': '1', 'nohp': '1', 'gender': 'LAKI-LAKI', 'place_of_birth': 'A', 'date_of_birth': date(2000, 1, 1), 'profession': 'BUMN', 'address': 'A', 'address_province': 'ACEH', 'address_regency': 'KABUPATEN SIMEULEU', 'address_district': 'TEUPAH SELATAN', 'address_village': 'LATIUNG', 'medical_blood_type': 'A', 'medical_rhesus': '+', 'medical_weight': 1, 'medical_height': 1, 'agree_to_the_terms_and_conditions': 'on', 'timestamp_last_request': datetime(2020, 12, 12, 1, 1, 1), 'file_pcr_positive': file1, 'file_pcr_negative': file2, 'disease_history': 'A', 'date_pcr_positive': date(2000, 12, 12), 'date_pcr_negative': date(2000, 12, 1)})

        response = self.client.get(reverse('daftar:daftarPendonor'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'formPendonor.html')

        file1 = SimpleUploadedFile("test.pdf", b"content", content_type="pdf")
        file2 = SimpleUploadedFile("test.pdf", b"content", content_type="pdf")
        response = self.client.post("%s?wid=%s" % (reverse('daftar:daftarPendonor'), 'pasien@test.com'), {'user': str(self.pendonor.id), 'is_requesting': 'on', 'full_name': 'Test', 'nik': '12', 'nohp': '1', 'gender': 'LAKI-LAKI', 'place_of_birth': 'A', 'date_of_birth': date(2000, 1, 1), 'profession': 'BUMN', 'address': 'A', 'address_province': 'ACEH', 'address_regency': 'KABUPATEN SIMEULEU', 'address_district': 'TEUPAH SELATAN', 'address_village': 'LATIUNG', 'medical_blood_type': 'A', 'medical_rhesus': '+', 'medical_weight': 1, 'medical_height': 1, 'agree_to_the_terms_and_conditions': 'on', 'timestamp_last_request': datetime(2020, 12, 12, 1, 1, 1), 'file_pcr_positive': file1, 'file_pcr_negative': file2, 'disease_history': 'A', 'date_pcr_positive': date(2000, 12, 12), 'date_pcr_negative': date(2000, 12, 1)})

    def test_models(self):
        # PASIEN1
        self.client.login(email="pasien@test.com", password="pasien")
        response = self.client.get(reverse('daftar:daftar'))
        self.assertEqual(response.status_code, 302)

        file = SimpleUploadedFile("test.pdf", b"content", content_type="pdf")
        response = self.client.post(reverse('daftar:daftarPasien'), {'user': str(self.pasien.id), 'is_requesting': 'on', 'full_name': 'Test', 'nik': '1', 'nohp': '1', 'gender': 'LAKI-LAKI', 'place_of_birth': 'A', 'date_of_birth': date(2000, 1, 1), 'profession': 'BUMN', 'address': 'A', 'address_province': 'ACEH', 'address_regency': 'KABUPATEN SIMEULEU', 'address_district': 'TEUPAH SELATAN', 'address_village': 'LATIUNG', 'medical_blood_type': 'A', 'medical_rhesus': '+', 'medical_weight': 1, 'medical_height': 1, 'agree_to_the_terms_and_conditions': 'on', 'timestamp_last_request': datetime(2020, 12, 12, 1, 1, 1), 'file': file})
        self.assertEqual(response.status_code, 302)

        self.assertEqual("Test", str(Pasien.objects.get(pk=1)))
        self.assertEqual("Test", Pasien.objects.get(pk=1).get_nickname())
        self.assertEqual("A, TEUPAH SELATAN, KABUPATEN SIMEULEU, ACEH", Pasien.objects.get(pk=1).get_full_address())
        self.assertEqual("A+", Pasien.objects.get(pk=1).get_full_blood_type())
        self.assertEqual(21, Pasien.objects.get(pk=1).age)

        # PENDONOR
        self.client.login(email="pendonor@test.com", password="pendonor")
        response = self.client.get(reverse('daftar:daftarPendonor'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'formPendonor.html')

        file1 = SimpleUploadedFile("test.pdf", b"content", content_type="pdf")
        file2 = SimpleUploadedFile("test.pdf", b"content", content_type="pdf")
        response = self.client.post("%s?wid=%s" % (reverse('daftar:daftarPendonor'), 'pasien@test.com'), {'user': str(self.pendonor.id), 'is_requesting': 'on', 'full_name': 'Test', 'nik': '1', 'nohp': '1', 'gender': 'LAKI-LAKI', 'place_of_birth': 'A', 'date_of_birth': date(2000, 1, 1), 'profession': 'BUMN', 'address': 'A', 'address_province': 'ACEH', 'address_regency': 'KABUPATEN SIMEULEU', 'address_district': 'TEUPAH SELATAN', 'address_village': 'LATIUNG', 'medical_blood_type': 'A', 'medical_rhesus': '+', 'medical_weight': 1, 'medical_height': 1, 'agree_to_the_terms_and_conditions': 'on', 'timestamp_last_request': datetime(2020, 12, 12, 1, 1, 1), 'file_pcr_positive': file1, 'file_pcr_negative': file2, 'disease_history': 'A', 'date_pcr_positive': date(2000, 12, 12), 'date_pcr_negative': date(2000, 12, 1)})
        self.assertEqual(response.status_code, 302)
            
        self.assertEqual("Test", str(Pendonor.objects.get(pk=2)))
        self.assertEqual("Test", Pendonor.objects.get(pk=2).get_nickname())
        self.assertEqual("A, TEUPAH SELATAN, KABUPATEN SIMEULEU, ACEH", Pendonor.objects.get(pk=2).get_full_address())
        self.assertEqual("A+", Pendonor.objects.get(pk=2).get_full_blood_type())

        response = self.client.post("%s?wid=%s" % (reverse('daftar:daftarPendonor'), 'pasien@test.com'), {'user': str(self.pendonor.id), 'is_requesting': 'on', 'full_name': 'Aa Test', 'nik': '1', 'nohp': '1', 'gender': 'LAKI-LAKI', 'place_of_birth': 'A', 'date_of_birth': date(2000, 1, 1), 'profession': 'BUMN', 'address': 'A', 'address_province': 'ACEH', 'address_regency': 'KABUPATEN SIMEULEU', 'address_district': 'TEUPAH SELATAN', 'address_village': 'LATIUNG', 'medical_blood_type': 'A', 'medical_rhesus': '+', 'medical_weight': 1, 'medical_height': 1, 'agree_to_the_terms_and_conditions': 'on', 'timestamp_last_request': datetime(2020, 12, 12, 1, 1, 1), 'file_pcr_positive': file1, 'file_pcr_negative': file2, 'disease_history': 'A', 'date_pcr_positive': date(2000, 12, 12), 'date_pcr_negative': date(2000, 12, 1)})
        self.assertEqual(response.status_code, 302)
        self.assertEqual("Aa", Pendonor.objects.get(pk=2).get_nickname())

        # PASIEN2
        self.client.login(email="pasien@test.com", password="pasien")
        response = self.client.get(reverse('daftar:daftarPasien'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'formPasien.html')

        file = SimpleUploadedFile("test.pdf", b"content", content_type="pdf")
        response = self.client.post(reverse('daftar:daftarPasien'), {'user': str(self.pasien.id), 'is_requesting': 'on', 'full_name': 'Aa Bb', 'nik': '1', 'nohp': '1', 'gender': 'LAKI-LAKI', 'place_of_birth': 'A', 'date_of_birth': date(2000, 1, 1), 'profession': 'BUMN', 'address': 'A', 'address_province': 'ACEH', 'address_regency': 'KABUPATEN SIMEULEU', 'address_district': 'TEUPAH SELATAN', 'address_village': 'LATIUNG', 'medical_blood_type': 'A', 'medical_rhesus': '+', 'medical_weight': 1, 'medical_height': 1, 'agree_to_the_terms_and_conditions': 'on', 'timestamp_last_request': datetime(2020, 12, 12, 1, 1, 1), 'file': file})
        self.assertEqual(response.status_code, 302)

        self.assertEqual("Aa", Pasien.objects.get(pk=1).get_nickname())
        self.assertEqual("Donor requested by Aa", str(DonorApplication.objects.get(pk=1)))
        DonorApplication.objects.get(id=1).delete()
