from django.forms import ModelForm, DateInput
from .models import *

class DateInput(DateInput):
    input_type = 'date'

class PasienForm(ModelForm):
	class Meta:
		model = Pasien
		fields = "__all__"
		labels = {
            'full_name': 'Nama Lengkap',
            'user': '',
            'timestamp_last_request': '',
            'is_requesting': '',
            'nik': 'NIK',
            'nohp': 'No. HP',
            'gender': 'Jenis Kelamin',
            'place_of_birth': 'Tempat Lahir',
            'date_of_birth': 'Tanggal Lahir',
            'profession': 'Pekerjaan',
            'address': 'Alamat',
            'address_province': 'Provinsi',
            'address_regency': 'Kota/Kabupaten',
            'address_district': 'Kecamatan',
            'address_village': 'Desa/Kelurahan',
            'medical_blood_type': 'Golongan Darah',
            'medical_rhesus': 'Rhesus',
            'medical_weight': 'Berat Badan',
            'medical_height': 'Tinggi Badan',
            'agree_to_the_terms_and_conditions': 'Dengan ini saya menyatakan bersedia untuk menjadi penerima donor plasma konvalesen dan siap dihubungi oleh Tim SIDOPLAK'
        }
		widgets = {
            'date_of_birth': DateInput()
        }
    
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		for field in iter(self.fields):
			self.fields[field].widget.attrs.update({
                'class':'form-text-control'
            })
		self.fields['file'].widget.attrs.update({
            'class':'custom-file-input',
            'required': True
        })
		self.fields['date_of_birth'].widget.attrs.update({
            'type':'date'
        })
		self.fields['agree_to_the_terms_and_conditions'].widget.attrs.update({
            'class': '',
            'required': True
        })
		for field in ['address_regency', 'address_district', 'address_village']:
			self.fields[field].widget.attrs.update({
                'disabled': True
            })
		for field in ['user', 'timestamp_last_request', 'is_requesting']:
			self.fields[field].widget.attrs.update({
                'hidden': True
            })

class PendonorForm(ModelForm):
	class Meta:
		model = Pendonor
		fields = "__all__"
		labels = {
            'full_name': 'Nama Lengkap',
            'user': '',
            'nik': 'NIK',
            'nohp': 'No. HP',
            'gender': 'Jenis Kelamin',
            'place_of_birth': 'Tempat Lahir',
            'date_of_birth': 'Tanggal Lahir',
            'profession': 'Pekerjaan',
            'address': 'Alamat',
            'address_province': 'Provinsi',
            'address_regency': 'Kota/Kabupaten',
            'address_district': 'Kecamatan',
            'address_village': 'Desa/Kelurahan',
            'medical_blood_type': 'Golongan Darah',
            'medical_rhesus': 'Rhesus',
            'medical_weight': 'Berat Badan',
            'medical_height': 'Tinggi Badan',
            'disease_history': 'Tuliskan seluruh riwayat penyakit yang pernah dialami:',
            'agree_to_the_terms_and_conditions': 'Dengan ini saya menyatakan bersedia untuk menjadi pendonor plasma konvalesen dan siap dihubungi oleh Tim SIDOPLAK',
            'timestamp_last_request': '',
            'file_pcr_positive': 'File PCR Positif',
            'file_pcr_negative': 'File PCR Negatif',
            'date_pcr_positive': 'Tanggal pertama kali positif Covid-19',
            'date_pcr_negative': 'Tanggal dinyatakan sembuh Covid-19',
            'has_donor': '',
        }
		widgets = {
            'date_of_birth': DateInput(),
            'date_pcr_positive': DateInput(),
            'date_pcr_negative': DateInput(),
        }
    
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		for field in iter(self.fields):
			self.fields[field].widget.attrs.update({
                'class':'form-text-control'
            })
		self.fields['date_of_birth'].widget.attrs.update({
            'type':'date'
        })
		self.fields['agree_to_the_terms_and_conditions'].widget.attrs.update({
            'class': '',
            'required': True
        })
		for field in ['address_regency', 'address_district', 'address_village']:
			self.fields[field].widget.attrs.update({
                'disabled': True
            })
		for field in ['file_pcr_negative', 'file_pcr_positive']:
			self.fields[field].widget.attrs.update({
				'class':'custom-file-input',
                'required': True
            })
		for field in ['user', 'timestamp_last_request', 'has_donor']:
			self.fields[field].widget.attrs.update({
                'hidden': True
            })
