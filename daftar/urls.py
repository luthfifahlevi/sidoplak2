from django.urls import path

from . import views

app_name = 'daftar'

urlpatterns = [
    path('', views.daftar, name='daftar'),
    path('pasien', views.daftarPasien, name='daftarPasien'),
    path('pendonor', views.daftarPendonor, name='daftarPendonor'),
]