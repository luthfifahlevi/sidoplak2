from datetime import datetime
from django.core.validators import FileExtensionValidator
from django.db.models import Model, OneToOneField, CharField, DateField, TextField, IntegerField, DateTimeField, FileField, BooleanField, CASCADE
from django.db.models.fields.related import ForeignKey
from django.utils import timezone, dateformat
from users.models import CustomUser

GENDER_CHOICES = (('', '*-- Pilih --*'), ('LAKI-LAKI', 'LAKI-LAKI'), ('PEREMPUAN', 'PEREMPUAN'),)
PROFESSION_CHOICES = (('', '*-- Pilih --*'), ('BUMN', 'BUMN'), ('MAHASISWA', 'MAHASISWA'), ('PEDAGANG', 'PEDAGANG'), ('PEG. NEGERI', 'PEG. NEGERI'), ('PEG. SWASTA', 'PEG. SWASTA'), ('PELAJAR', 'PELAJAR'), ('PETANI / BURUH', 'PETANI / BURUH'), ('POLRI', 'POLRI'), ('TNI', 'TNI'), ('WIRASWASTA', 'WIRASWASTA'), ('LAIN-LAIN', 'LAIN-LAIN'),)
BLOOD_TYPE_CHOICES = (('', '*-- Pilih --*'), ('A', 'A'), ('B', 'B'), ('O', 'O'), ('AB', 'AB'),)
RHESUS_CHOICES = (('', '*-- Pilih --*'), ('+', '+'), ('-', '-'),)

def uploaded_file_directory_path(instance, filename):
	filename_mime_type = filename[filename.rfind("."):]
	id = instance.pk
	return f"donor-request/donor-request-{id}{filename_mime_type}"

def uploaded_file_pcr_positive_directory_path(instance, filename):
	filename_mime_type = filename[filename.rfind("."):]
	id = instance.user.email + "-" + dateformat.format(timezone.localtime(timezone.now()), 'dmY-His')
	id = id.replace("@", "-").replace(".", "-")
	return f"donor/pcr-positive/{id}_pcr-positive{filename_mime_type}"

def uploaded_file_pcr_negative_directory_path(instance, filename):
	filename_mime_type = filename[filename.rfind("."):]
	id = instance.user.email + "-" + dateformat.format(timezone.localtime(timezone.now()), 'dmY-His')
	id = id.replace("@", "-").replace(".", "-")
	return f"donor/pcr-negative/{id}_pcr-negative{filename_mime_type}"

class Pasien(Model):
	user = OneToOneField(CustomUser, on_delete=CASCADE, related_name='pasien', primary_key=True)
	full_name = CharField(max_length = 255)
	nik = CharField(max_length = 255)
	nohp = CharField(max_length = 255)
	gender = CharField(max_length = 255, choices = GENDER_CHOICES)
	place_of_birth = CharField(max_length = 255)
	date_of_birth = DateField()
	profession = CharField(max_length = 255, choices = PROFESSION_CHOICES)
	address = TextField()
	address_province = CharField(max_length = 255)
	address_regency = CharField(max_length = 255)
	address_district = CharField(max_length = 255)
	address_village = CharField(max_length = 255)
	medical_blood_type = CharField(max_length = 2, choices = BLOOD_TYPE_CHOICES)
	medical_rhesus = CharField(max_length = 1, choices = RHESUS_CHOICES)
	medical_weight = IntegerField()
	medical_height = IntegerField()

	timestamp_last_request = DateTimeField()
	is_requesting = BooleanField(default=False)

	# Ga kepake, cuma biar gampang munculin form file di pendaftaran. 
	# File aslinya kesimpen di class DonorApplication.
	file = FileField(upload_to=uploaded_file_directory_path, 
                        validators=[FileExtensionValidator(allowed_extensions=['pdf'])],
                        null=True, blank=True
                        )
	agree_to_the_terms_and_conditions = BooleanField(default=False)

	@property
	def age(self):
		return int((datetime.now().date() - self.date_of_birth).days / 365.25)

	def __str__(self):
		return self.full_name

	def get_nickname(self):
		idx_first_space = self.full_name.find(" ")
		if idx_first_space != -1:
			return self.full_name[:idx_first_space].capitalize()
		else:
			return self.full_name.capitalize()

	def get_full_address(self):
		return f"{self.address}, {self.address_district}, {self.address_regency}, {self.address_province}"

	def get_full_blood_type(self):
		return f"{self.medical_blood_type}{self.medical_rhesus}"


class Pendonor(Model):
	user = OneToOneField(CustomUser, on_delete=CASCADE, related_name='pendonor', primary_key=True)
	full_name = CharField(max_length = 255)
	nik = CharField(max_length = 255)
	nohp = CharField(max_length = 255)
	gender = CharField(max_length = 255, choices = GENDER_CHOICES)
	place_of_birth = CharField(max_length = 255)
	date_of_birth = DateField()
	profession = CharField(max_length = 255, choices = PROFESSION_CHOICES)
	address = TextField()
	address_province = CharField(max_length = 255)
	address_regency = CharField(max_length = 255)
	address_district = CharField(max_length = 255)
	address_village = CharField(max_length = 255)
	medical_blood_type = CharField(max_length = 2, choices = BLOOD_TYPE_CHOICES)
	medical_rhesus = CharField(max_length = 1, choices = RHESUS_CHOICES)
	medical_weight = IntegerField()
	medical_height = IntegerField()
	disease_history = CharField(max_length=255, null=True)

	timestamp_last_request = DateTimeField(default=timezone.now)
	has_donor = BooleanField(default=False)

	date_pcr_positive = DateField()
	file_pcr_positive = FileField(upload_to=uploaded_file_pcr_positive_directory_path, 
							validators=[FileExtensionValidator(allowed_extensions=['pdf'])],
							null=True, blank=True
							)
	date_pcr_negative = DateField()
	file_pcr_negative = FileField(upload_to=uploaded_file_pcr_negative_directory_path, 
							validators=[FileExtensionValidator(allowed_extensions=['pdf'])],
							null=True, blank=True
							)
	agree_to_the_terms_and_conditions = BooleanField(default=False)

	def __str__(self):
		return self.full_name

	def get_nickname(self):
		idx_first_space = self.full_name.find(" ")
		if idx_first_space != -1:
			return self.full_name[:idx_first_space].capitalize()
		else:
			return self.full_name.capitalize()

	def get_full_address(self):
		return f"{self.address}, {self.address_district}, {self.address_regency}, {self.address_province}"

	def get_full_blood_type(self):
		return f"{self.medical_blood_type}{self.medical_rhesus}"



class DonorApplicationStatus(Model):
	status = CharField(max_length = 2, primary_key=True)
	message = CharField(max_length = 50)
	next = OneToOneField('self', related_name='prev', on_delete = CASCADE, null=True, blank=True)

class DonorApplication(Model):
	patient = ForeignKey(Pasien, on_delete=CASCADE, related_name='donor_applications')
	donor = ForeignKey(Pendonor, on_delete=CASCADE, related_name='donor_applications', blank=True, null=True)
	status = ForeignKey(DonorApplicationStatus, on_delete = CASCADE, related_name='patient_requests', null=True)
	timestamp_created = DateTimeField()
	file = FileField(upload_to=uploaded_file_directory_path, 
							validators=[FileExtensionValidator(allowed_extensions=['pdf'])],
							null=True, blank=True
							)
	
	def __str__(self):
		return "Donor requested by " + self.patient.get_nickname()

	def delete(self):
		self.file.delete()
		super().delete()