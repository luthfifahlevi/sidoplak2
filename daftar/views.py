from daftar.forms import PasienForm, PendonorForm
from daftar.models import DonorApplication, DonorApplicationStatus, Pasien, Pendonor
from django.shortcuts import redirect, render
from django.utils import timezone

def daftar(request):
    
    if request.user.role == 'PASIEN':
        return redirect('/daftar/pasien')

    return redirect('/daftarPasien')

def daftarPasien(request):

    if request.user.role != 'PASIEN':
        return render(request, '404.html')

    oldPas = Pasien.objects.filter(user=request.user)

    form = PasienForm(request.POST or None)
    form.fields['user'].initial = request.user
    form.fields['timestamp_last_request'].initial = timezone.now()
    form.fields['is_requesting'].widget.attrs.update({
        'checked': True
    })

    if request.method == "POST":

        if len(oldPas) > 0:
            oldPas.update(
                full_name=request.POST['full_name'],
                nik=request.POST['nik'],
                nohp=request.POST['nohp'],
                gender=request.POST['gender'],
                place_of_birth=request.POST['place_of_birth'],
                date_of_birth=request.POST['date_of_birth'],
                profession=request.POST['profession'],
                address=request.POST['address'],
                address_province=request.POST['address_province'],
                address_regency=request.POST['address_regency'],
                address_district=request.POST['address_district'],
                address_village=request.POST['address_village'],
                medical_blood_type=request.POST['medical_blood_type'],
                medical_rhesus=request.POST['medical_rhesus'],
                medical_weight=request.POST['medical_weight'],
                medical_height=request.POST['medical_height'],
                timestamp_last_request=timezone.now()
            )

            oldApp = DonorApplication.objects.filter(patient=oldPas.first()).first()
            oldApp.timestamp=timezone.now()
            oldApp.save()

        else:

            file = request.FILES['file']
            if (file.name.split('.')[-1] != 'pdf') or (file.size > 100000000):
                return render(request, 'formPasien.html', {'form': form, 'errorFile': 'Perhatikan ketentuan dokumen dengan benar.'})

            if form.is_valid():
                
                form.save()
                newPas = Pasien.objects.get(user=request.user)

                newApp = DonorApplication(patient=newPas, timestamp_created=timezone.now())
                newApp.save()
                newApp.file = file
                newApp.status = DonorApplicationStatus.objects.get(status="1")
                newApp.save()
            else:
                return render(request, 'formPasien.html', {'form': form})
        
        return redirect('/?success=1')
    
    filename = 'File Permohonan Donor'
    if len(oldPas) > 0:
        oldPas = oldPas.first()
        oldApp = DonorApplication.objects.get(patient=oldPas)
        form.fields['full_name'].initial = oldPas.full_name
        form.fields['nik'].initial = oldPas.nik
        form.fields['nohp'].initial = oldPas.nohp
        form.fields['gender'].initial = oldPas.gender
        form.fields['place_of_birth'].initial = oldPas.place_of_birth
        form.fields['date_of_birth'].initial = oldPas.date_of_birth
        form.fields['profession'].initial = oldPas.profession
        form.fields['address'].initial = oldPas.address
        form.fields['address_province'].initial = oldPas.address_province
        form.fields['address_regency'].initial = oldPas.address_regency
        form.fields['address_district'].initial = oldPas.address_district
        form.fields['address_village'].initial = oldPas.address_village
        form.fields['medical_blood_type'].initial = oldPas.medical_blood_type
        form.fields['medical_rhesus'].initial = oldPas.medical_rhesus
        form.fields['medical_weight'].initial = oldPas.medical_weight
        form.fields['medical_height'].initial = oldPas.medical_height
        form.fields['file'].widget.attrs.update({
            'disabled': True,
        })
        filename = oldApp.file.name[(oldApp.file.name.find('/') + 1):]
    
    return render(request, 'formPasien.html', {'form': form, 'namaFile': filename})

def daftarPendonor(request):

    if request.user.role != 'PENDONOR':
        return render(request, '404.html')

    oldPen = Pendonor.objects.filter(user=request.user)

    form = PendonorForm(request.POST or None)
    form.fields['user'].initial = request.user
    form.fields['timestamp_last_request'].initial = timezone.now()

    if request.method == "POST":

        if len(oldPen) > 0:
            oldPen.update(
                full_name=request.POST['full_name'],
                nik=request.POST['nik'],
                nohp=request.POST['nohp'],
                gender=request.POST['gender'],
                place_of_birth=request.POST['place_of_birth'],
                date_of_birth=request.POST['date_of_birth'],
                profession=request.POST['profession'],
                address=request.POST['address'],
                address_province=request.POST['address_province'],
                address_regency=request.POST['address_regency'],
                address_district=request.POST['address_district'],
                address_village=request.POST['address_village'],
                medical_blood_type=request.POST['medical_blood_type'],
                medical_rhesus=request.POST['medical_rhesus'],
                medical_weight=request.POST['medical_weight'],
                medical_height=request.POST['medical_height'],
                disease_history=request.POST['disease_history'],
                date_pcr_positive=request.POST['date_pcr_positive'],
                date_pcr_negative=request.POST['date_pcr_negative'],
                timestamp_last_request=timezone.now()
            )
        
        else:

            file1 = request.FILES['file_pcr_positive']
            file2 = request.FILES['file_pcr_negative']

            if (file1.name.split('.')[-1] != 'pdf') or (file1.size > 100000000) or (file2.name.split('.')[-1] != 'pdf') or (file2.size > 100000000):
                return render(request, 'formPendonor.html', {'form': form, 'errorFile': 'Perhatikan ketentuan dokumen dengan benar.'})

            if form.is_valid():

                form.save()
                newPen = Pendonor.objects.get(user=request.user)
                newPen.save()
                newPen.file_pcr_positive = file1
                newPen.file_pcr_negative = file2
                newPen.save()
            else:
                return render(request, 'formPendonor.html', {'form': form})
        
        wid = request.GET['wid']
        pasien = Pasien.objects.get(user__email=wid)
        pendonor = Pendonor.objects.get(user=request.user)
        app = DonorApplication.objects.get(patient=pasien)
        app.donor = pendonor
        app.status = DonorApplicationStatus.objects.get(status="2")
        app.save()

        return redirect('/?success=2')

    filename1 = 'File PCR Positif'
    filename2 = 'File PCR Negatif'
    if len(oldPen) > 0:
        oldPen = oldPen.first()
        form.fields['full_name'].initial = oldPen.full_name
        form.fields['nik'].initial = oldPen.nik
        form.fields['nohp'].initial = oldPen.nohp
        form.fields['gender'].initial = oldPen.gender
        form.fields['place_of_birth'].initial = oldPen.place_of_birth
        form.fields['date_of_birth'].initial = oldPen.date_of_birth
        form.fields['profession'].initial = oldPen.profession
        form.fields['address'].initial = oldPen.address
        form.fields['address_province'].initial = oldPen.address_province
        form.fields['address_regency'].initial = oldPen.address_regency
        form.fields['address_district'].initial = oldPen.address_district
        form.fields['address_village'].initial = oldPen.address_village
        form.fields['medical_blood_type'].initial = oldPen.medical_blood_type
        form.fields['medical_rhesus'].initial = oldPen.medical_rhesus
        form.fields['medical_weight'].initial = oldPen.medical_weight
        form.fields['medical_height'].initial = oldPen.medical_height
        form.fields['disease_history'].initial = oldPen.disease_history
        form.fields['date_pcr_positive'].initial = oldPen.date_pcr_positive
        form.fields['date_pcr_negative'].initial = oldPen.date_pcr_negative
        form.fields['file_pcr_positive'].widget.attrs.update({
            'disabled': True,
        })
        form.fields['file_pcr_negative'].widget.attrs.update({
            'disabled': True,
        })
        filename1 = oldPen.file_pcr_positive.name[(oldPen.file_pcr_positive.name.find('/', oldPen.file_pcr_positive.name.find('/') + 1) + 1):]
        filename2 = oldPen.file_pcr_negative.name[(oldPen.file_pcr_negative.name.find('/', oldPen.file_pcr_negative.name.find('/') + 1) + 1):]
    
    return render(request, 'formPendonor.html', {'form': form, 'namaFile1': filename1, 'namaFile2': filename2})