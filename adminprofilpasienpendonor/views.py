from django.shortcuts import render, redirect
from django.http import Http404, HttpResponse
from django.conf import settings
from django.contrib import messages

from daftar.models import Pasien, Pendonor
from .forms import FiltersForm

import zipfile
import io
import os
import urllib.parse


def admin_list_patient_donor(request):
	if not request.user.is_admin():
		raise Http404

	#GET parameters init
	page = 1
	show = 10
	fil_prov = None
	fil_goldar = None
	fil_role = None
	search_name = None

	url_param = "" #Create URL get parameter for page viewing

	if request.GET:
		if "page" in request.GET:
			if request.GET['page'].isdigit():
				page = int(request.GET['page'])
		if "fil_role" in request.GET:
			fil_role = request.GET['fil_role']
			url_param += f"fil_role={fil_role}&"
		if "show" in request.GET:
			if request.GET['show'].isdigit():
				show = int(request.GET['show'])
				url_param += f"show={str(show)}&"
			elif request.GET['show'] == "semua":
				if fil_role == "PENDONOR":
					show = Pendonor.objects.count()
				else:
					show = Pasien.objects.count()
				url_param += f"show=semua&"
		if "fil_prov" in request.GET:
			fil_prov = request.GET['fil_prov']
			url_param += f"fil_prov={fil_prov}&"
		if "fil_goldar" in request.GET:
			fil_goldar = request.GET['fil_goldar']
			url_param += f"fil_goldar={fil_goldar}&"
		if "search_name" in request.GET:
			search_name = request.GET['search_name']
			url_param += f"search_name={search_name}&"
	elif request.POST:
		page = 1
		if "fil_role" in request.POST:
			if request.POST['fil_role'] != "":
				fil_role = request.POST['fil_role']
				url_param += f"fil_role={fil_role}&"
		if "show" in request.POST:
			if request.POST['show'] != "" and request.POST['show'].isdigit():
				show = int(request.POST['show'])
				url_param += f"show={str(show)}&"
			elif request.POST['show'] == "semua":
				if fil_role == "PENDONOR":
					show = Pendonor.objects.count()
				else:
					show = Pasien.objects.count()
				url_param += f"show=semua&"
		if "fil_prov" in request.POST:
			if request.POST['fil_prov'] != "":
				fil_prov = request.POST['fil_prov']
				url_param += f"fil_prov={fil_prov}&"
		if "fil_goldar" in request.POST:
			if request.POST['fil_goldar'] != "":
				fil_goldar = request.POST['fil_goldar']
				url_param += f"fil_goldar={fil_goldar}&"
		if "search_name" in request.POST:
			if request.POST['search_name'] != "":
				search_name = request.POST['search_name']
				url_param += f"search_name={search_name}&"

	url_param = url_param.replace("+", "%2B") #URL encoding

	if show <= 0:
		show = 1


	#set filter for query
	if fil_role == "PENDONOR":
		# filter on Pendonor model
		users_to_be_shown = Pendonor.objects.all()
	else:
		# filter on Pasien model
		users_to_be_shown = Pasien.objects.all()

	if fil_prov is not None:
		users_to_be_shown = users_to_be_shown.filter(address_province=fil_prov)
	if fil_goldar is not None:
		goldar = fil_goldar[:-1]
		rhesus = fil_goldar[-1:]
		users_to_be_shown = users_to_be_shown.filter(medical_blood_type=goldar).filter(medical_rhesus=rhesus)
	if search_name is not None:
		users_to_be_shown = users_to_be_shown.filter(full_name__icontains=search_name)

	#number of pages
	num_of_users_to_be_shown = users_to_be_shown.count()
	num_of_pages = int(num_of_users_to_be_shown / show) + (num_of_users_to_be_shown % show > 0)

	# If page requested is out of the available pages, raise Http404
	if (page <= 0 or page > num_of_pages) and num_of_pages != 0:
		raise Http404

	list_page_number_bar = []
	for i in range(-1, 2):
		page_number_bar = page + i
		if page_number_bar > 0 and page_number_bar <= num_of_pages:
			list_page_number_bar.append(page_number_bar)

	is_there_more_pages_left = False
	is_there_more_pages_right = False
	if (page - 2) > 0:
		is_there_more_pages_left = True
	if (page + 2) <= num_of_pages:
		is_there_more_pages_right = True

	# Get objects
	idx_start = show * (page-1)
	idx_end = show * page
	if fil_role == "PENDONOR":
		# if role is pendonor, set result to donors and let patients be None
		donors = users_to_be_shown.select_related('user').order_by("-timestamp_last_request")[idx_start:idx_end]
		patients = None
	else:
		# if role is not pendonor, set result to patients and let donors be None
		donors = None
		patients = users_to_be_shown.select_related('user').order_by("-timestamp_last_request")[idx_start:idx_end]


	# Filter Form
	form = FiltersForm(request.POST or None)
	
	if show == num_of_users_to_be_shown:
		form.fields['show'].initial = "semua"
	else:
		form.fields['show'].initial = show
	if fil_prov is not None:
		form.fields['fil_prov'].initial = fil_prov
	if fil_goldar is not None:
		form.fields['fil_goldar'].initial = fil_goldar
	if fil_role == "PENDONOR":
		form.fields['fil_role'].initial = "PENDONOR"
	else:
		form.fields['fil_role'].initial = "PASIEN"
	if search_name is not None:
		form.fields['search_name'].initial = search_name

	context = {
		'form' : form,
		'patients' : patients,
		'donors' : donors,
		'list_page_number_bar' : list_page_number_bar,
		'page' : page,
		'num_of_pages' : num_of_pages,
		'url_param' : url_param,
		'is_there_more_pages_left' : is_there_more_pages_left,
		'is_there_more_pages_right' : is_there_more_pages_right,
	}

	return render(request, "adminprofilpasienpendonor/admin_list_patient_donor.html", context)



def patient_profile(request):
	if not request.user.is_admin():
		raise Http404

	pk = None
	if "pk" in request.GET:
		pk = request.GET['pk']

	patient = Pasien.objects.filter(user__email=pk).select_related('user')
	if patient:
		patient = patient.first()
		donor_applications = patient.donor_applications.order_by("-timestamp_created")
	else:
		raise Http404

	return render(request, "adminprofilpasienpendonor/patient_profile.html", {
		'patient' : patient,
		'donor_applications' : donor_applications,
	})



def donor_profile(request):
	if not request.user.is_admin():
		raise Http404

	pk = None
	if "pk" in request.GET:
		pk = request.GET['pk']


	donor = Pendonor.objects.filter(user__email=pk)
	if donor:
		donor = donor.first()
	else:
		raise Http404

	return render(request, "adminprofilpasienpendonor/donor_profile.html", {
		'donor' : donor,
	})



def download(request, file_name):
	if not request.user.is_superuser:
		raise Http404

	file_name = urllib.parse.unquote(file_name)
	file_name = urllib.parse.unquote(file_name)
	file_name = file_name.replace("/media/", "", 1)

	if os.path.isfile(settings.MEDIA_ROOT / file_name):
		zip_subdir = file_name[file_name.rfind("/")+1:file_name.rfind(".")].replace("/", "_")
		zip_filename = "%s.zip" % zip_subdir
		return download_as_zip([file_name], zip_subdir, zip_filename)
	else:
		return HttpResponse("FILE NOT FOUND: Heroku tidak mendukung unggahan file lama karena akan dihapus secara berkala pada filesystemnya.")




''' CUSTOM FUNCTION '''

def download_as_zip(filenames, zip_subdir, zip_filename):
	# Open StringIO to grab in-memory ZIP contents
	s = io.BytesIO()

	# The zip compressor
	zf = zipfile.ZipFile(s, "w")

	media_root = settings.MEDIA_ROOT
	for fpath in filenames:
		fpath = media_root / fpath
		# Calculate path for file in zip
		fdir, fname = os.path.split(fpath)
		zip_path = os.path.join(zip_subdir, fname)

		# Add file, at correct path
		zf.write(fpath, zip_path)

	# Must close zip for all contents to be written
	zf.close()

	# Grab ZIP file from in-memory, make response with correct MIME-type
	response = HttpResponse(s.getvalue(), content_type = "application/x-zip-compressed")
	response['Content-Disposition'] = 'attachment; filename=%s' % zip_filename

	return response