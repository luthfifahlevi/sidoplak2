from django.urls import path
from . import views

app_name = 'adminprofilpasienpendonor'

urlpatterns = [
	path('', views.admin_list_patient_donor, name='admin_list_patient_donor'),

	path('profil-pendonor/', views.donor_profile, name='donor_profile'),
	path('profil-pasien/', views.patient_profile, name='patient_profile'),

	path('download/<str:file_name>/', views.download, name='download'),
]