from django.shortcuts import render


def home(request):

    if request.GET.get('success'):
        return render(request, 'main/home.html', {'formSuccess': ('Form pendaftaran pendonor sudah diterima. Silahkan mengecek status verifikasi pada laman profil secara berkala.' if request.GET['success'] == '2' else 'Form pendaftaran permohonan donor sudah diterima. Silahkan mengecek status verifikasi pada laman profil secara berkala.')})    
    return render(request, 'main/home.html')
