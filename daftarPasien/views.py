from django.shortcuts import render
from django.http import Http404
from daftar.models import DonorApplication, DonorApplicationStatus, Pasien , Pendonor
from users.models import CustomUser
from .forms import FiltersForm
import copy

def daftarPasien(request):
	if not request.user.is_authenticated:
		raise Http404

	listPasien = listPasienStatusMencari(Pasien.objects.all())
	form = FiltersForm(request.POST or None)
	context = {
	    'listPasien' : listPasien,
		'form' : form,
	}
	return render(request, 'daftarPasien/daftarPasien.html', context)

def detailPasienMinimum(request):
    if not request.user.is_authenticated:
        raise Http404
    wid = request.GET['wid']
    wid = request.GET['wid']
    dataUser = Pasien.objects.filter(user__email=wid).select_related('user')
    is_Pendonor = 0
    tmp = Pendonor.objects.filter(user=request.user)
    if request.user.role == "PENDONOR" and len(tmp) == 0:
        is_Pendonor = 1
    elif request.user.role == "PENDONOR" and len(tmp) == 1:
        donorStatus = DonorApplication.objects.filter(donor=tmp[0])[0]
        if donorStatus.status.status == "1":
            is_Pendonor = 1
    context = {
        'is_Pendonor' : is_Pendonor,
        'dataUser' : dataUser[0],
        'wid' : wid,
    }
    return render(request,'daftarPasien/detailPasienMinimum.html',context)

def filter(request):
    form = FiltersForm(request.POST)
    if request.method == "POST" and form.is_valid():
        prov = None
        goldar = None
        if "fil_prov" in request.POST:
            prov = request.POST['fil_prov']
        if "fil_goldar" in request.POST:
            goldar = request.POST['fil_goldar']
        listPasien = []
        if goldar and prov:
            listPasien = listPasienStatusMencari(Pasien.objects.filter(address_province=prov,medical_blood_type=goldar[0],medical_rhesus=goldar[1]))
        elif not goldar and prov:
            listPasien = listPasienStatusMencari(Pasien.objects.filter(address_province=prov))
        elif not prov and goldar:
            listPasien = listPasienStatusMencari(Pasien.objects.filter(medical_blood_type=goldar[0],medical_rhesus=goldar[1]))
        else :
            listPasien = listPasienStatusMencari(Pasien.objects.all())
        form = FiltersForm(request.POST or None)
        context = {
            'listPasien' : listPasien,
            'form' : form,
        }
        return render(request, 'daftarPasien/daftarPasien.html', context)
    else:
        raise Http404

def listPasienStatusMencari(listPasien):
    listPasienKembalian = []
    for i in range (len(listPasien)):
        donorTmp = DonorApplication.objects.filter(patient=listPasien[i])
        if not donorTmp:
            continue
        else:
            status = donorTmp[0].status.status
            if status == "1":
                listPasienKembalian.append(listPasien[i])
    return listPasienKembalian
