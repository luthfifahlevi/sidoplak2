from django import forms

from adminprofilpasienpendonor.constants import get_address_province_as_form_choices
from users.models import USER_ROLE_CHOICES

ADDRESS_PROVINCE_FILTER_CHOICES = get_address_province_as_form_choices("--Provinsi--")
BLOOD_TYPE_FILTER_CHOICES = (('', "--Goldar--"), ('A+', "A+"), ('A-', "A-"), ('B+', "B+"), ('B-', "B-"), ('O+', "O+"), ('O-', "O-"), ('AB+', "AB+"), ('AB-', "AB-"),)


class FiltersForm(forms.Form):
	fil_prov = forms.ChoiceField(required = False, choices=ADDRESS_PROVINCE_FILTER_CHOICES, widget=forms.Select(attrs={'type' : 'text', 'class' : 'form-text-control'}))
	fil_goldar = forms.ChoiceField(required = False, choices=BLOOD_TYPE_FILTER_CHOICES, widget=forms.Select(attrs={'type' : 'text', 'class' : 'form-text-control'}))