from django.urls import path
from daftar.models import Pasien
from . import views

app_name = 'daftarPasien'

urlpatterns = [
	path('', views.daftarPasien, name='daftarPasien'),
	path('filter/', views.filter, name='filter'),
	path('detailPasienMinimum/', views.detailPasienMinimum, name='detailPasienMinimum'),
]